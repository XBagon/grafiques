use crate::Scene;
use nalgebra::*;
use rendy::hal::Backend;
use rendy::init::winit::{
    dpi::PhysicalPosition,
    event::{ElementState, KeyboardInput, MouseButton, VirtualKeyCode},
};

pub struct InputManager<B: Backend> {
    pub focused: bool,
    pub axes: Vector2<i8>,
    pub mouse_pos_delta: Vector2<f64>,
    pub mouse_wheel_delta: Vector2<f32>,
    pub mouse_click_handlers: Vec<Box<dyn Fn(&mut Scene<B>, ElementState, MouseButton)>>,
}

impl<B: Backend> Default for InputManager<B> {
    fn default() -> Self {
        Self {
            focused: false,
            axes: Vector2::zeros(),
            mouse_pos_delta: Vector2::zeros(),
            mouse_wheel_delta: Vector2::zeros(),
            mouse_click_handlers: Vec::new(),
        }
    }
}

impl<B: Backend> InputManager<B> {
    pub fn update_on_keyboard_input(&mut self, event: &KeyboardInput) {
        let pressed = event.state == ElementState::Pressed;
        if let Some(key) = event.virtual_keycode {
            match key {
                VirtualKeyCode::A => {
                    self.axes.x = -(pressed as i8);
                }
                VirtualKeyCode::D => {
                    self.axes.x = pressed as i8;
                }
                VirtualKeyCode::W => {
                    self.axes.y = -(pressed as i8);
                }
                VirtualKeyCode::S => {
                    self.axes.y = pressed as i8;
                }
                _ => {}
            }
        }
    }

    pub fn update_on_mouse_motion(&mut self, delta: (f64, f64)) {
        if self.focused {
            self.mouse_pos_delta[0] += delta.0;
            self.mouse_pos_delta[1] += delta.1;
        }
    }

    pub fn update_on_mouse_wheel(&mut self, delta: (f32, f32)) {
        if self.focused {
            self.mouse_wheel_delta[0] += delta.0;
            self.mouse_wheel_delta[1] += delta.1;
        }
    }

    pub fn clear_deltas(&mut self) {
        self.mouse_pos_delta = Vector2::zeros();
        self.mouse_wheel_delta = Vector2::zeros();
    }

    pub fn trigger_mouse_click(
        &mut self,
        scene: &mut Scene<B>,
        state: ElementState,
        button: MouseButton,
    ) {
        for handler in &self.mouse_click_handlers {
            handler(scene, state, button);
        }
    }
}
