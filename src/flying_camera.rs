use crate::input::InputManager;
use nalgebra::*;
use ncollide3d::query::Ray;
use rendy::hal::Backend;

#[derive(Debug)]
pub struct FlyingCamera {
    pub view: Isometry3<f32>,
    pub proj: Perspective3<f32>,
    pub speed: f32,
    pub speed_increase: f32,
    pub turn_speed: f32,
}

impl FlyingCamera {
    pub fn update<B: Backend>(&mut self, input: &InputManager<B>) {
        self.speed += input.mouse_wheel_delta[1] * self.speed_increase;

        self.view *= &Translation3::new(
            input.axes.x as f32 * self.speed,
            0.0,
            input.axes.y as f32 * self.speed,
        );
        self.view *= &UnitQuaternion::new(
            Vector3::new(
                input.mouse_pos_delta[1] as f32,
                -input.mouse_pos_delta[0] as f32,
                0.0,
            ) * self.turn_speed,
        );
    }

    pub fn look_ray(&self) -> Ray<f32> {
        Ray::new(
            self.view.translation.vector.into(),
            self.view.rotation.transform_vector(&-Vector3::z()),
        )
    }
}
