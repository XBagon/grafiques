use nalgebra::Point3;

pub fn convert_indices(indices: &Vec<Point3<u32>>) -> Vec<u32> {
    let mut result = Vec::new();
    for index in indices {
        let mut index_array: [u32; 3] = index.coords.into();
        result.extend_from_slice(&mut index_array);
    }
    result
}
